<?php

declare(strict_types=1);

namespace Drupal\webauthn_framework\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Webmozart\Assert\Assert;

/**
 * Module settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return 'webauthn_framework_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('webauthn_framework.settings');

    $form['disclose_registration'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disclose user account identifiers'),
      '#description' => $this->t("By default, users are not warned even when they provide an invalid userHandle. This is to increase security by preventing attackers from confirming whether a userHandle has been registered. However, this reduces usability because users may have mistyped the userHandle. Check this box if you want to disclose to users that accounts have been registered."),
      '#required' => FALSE,
      '#default_value' => $config->get('disclose_registration'),
    ];

    $form['relying_party'] = [
      '#type' => 'details',
      '#title' => $this->t('Relying party'),
      '#open' => TRUE,
      '#tree' => FALSE,
    ];

    $form['relying_party']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('The name for the site under the WebAuthn context. Defaults to Drupal site name.'),
      '#required' => TRUE,
      '#default_value' => $config->get('relying_party.name') ?? $this->config('system.site')
        ->get('name'),
    ];

    $form['relying_party']['id_auto'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically set ID to site domain'),
      '#description' => $this->t("Usually, the id is the site's current domain. This attempts to set the domain based on the Drupal request."),
      '#required' => FALSE,
      '#default_value' => $config->get('relying_party.id_auto'),
    ];

    $form['relying_party']['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID', [], ['context' => 'WebAuthn']),
      '#description' => $this->t('The rp ID shall be the domain of the application without the scheme, userinfo, port, path, or user. No IP addresses.'),
      '#required' => TRUE,
      '#default_value' => $config->get('relying_party.id'),
      '#states' => [
        'visible' => [
          'input[name="id_auto"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $logo = $config->get('relying_party.logo');
    $form['relying_party']['logo'] = [
      '#type' => 'file',
      '#upload_location' => 'private://',
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg', 'jpeg', 'ico', 'svg', 'png'],
      ],
      '#title' => $this->t('Logo'),
      '#description' => $this->t('(optional) The logo. Is stored using Base64 encoding. The icon may be ignored by browsers, especially if its length is greater than 128 bytes.'),
    ];

    if (is_string($logo) && $logo !== '') {
      $form_state->set('current_logo', $logo);
      $form['relying_party']['current_logo_header'] = [
        '#markup' => $this->t('Current logo') . '<br />',
      ];
      $form['relying_party']['current_logo'] = [
        '#type' => 'html_tag',
        '#tag' => 'img',
        '#attributes' => [
          'src' => $logo,
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $all_files = $this->getRequest()->files->get('files', []);
    if ($all_files['logo'] instanceof UploadedFile) {
      $file_upload = $all_files['logo'];
      if ($file_upload->isValid()) {
        $form_state->setValue('logo', $this->fileToBase64($file_upload));
      }
      else {
        $form_state->setErrorByName('logo', $this->t('The file could not be uploaded.'));
      }
    }
  }

  /**
   * Process uploaded file and store it as base64 encoded data.
   *
   * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file_upload
   *   Uploaded file.
   *
   * @return string
   *   The base64 encoded string from file.
   */
  private function fileToBase64(UploadedFile $file_upload): string {
    $data = file_get_contents($file_upload->getRealPath());
    Assert::stringNotEmpty($data, 'Failed to get file content when converting to base64!');
    return 'data:' . $file_upload->getClientMimeType() . ';base64,' . base64_encode($data);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $disclose_registration = $form_state->getValue('disclose_registration');
    $relying_party = [
      'name' => $form_state->getValue('name'),
      'id' => $form_state->getValue('id'),
      'id_auto' => $form_state->getValue('id_auto'),
      'logo' => $form_state->getValue('logo', $form_state->get('current_logo')),
    ];

    $this->config('webauthn_framework.settings')
      ->set('disclose_registration', $disclose_registration)
      ->set('relying_party', $relying_party)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function getEditableConfigNames(): array {
    return [
      'webauthn_framework.settings',
    ];
  }

}
