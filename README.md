# Webauthn Framework

## Overview

Provides the latest version of the webauthn-framework library as a Drupal
service. This is a module for developers and a dependency for other projects
that want to use Webauthn.

This module has no relationship to webauthn-framework; it implements the
framework, but it is not endorsed by webauthn-framework.


## Requirements

This module requires no modules outside of Drupal core.


## Recommended modules

[Decoupled Passkeys](https://www.drupal.org/project/decoupled_passkeys): Use
the Webauthn Framework in decoupled applications.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Administration » Configuration » People » Webauthn Framework.
1. Choose whether to use the site name for the relying party name or your own.
1. Choose whether to use the site domain as the ID or set your own.


## Maintainers

- Patrick Kenny - [ptmkenny](https://www.drupal.org/u/ptmkenny)
