<?php

declare(strict_types=1);

namespace Drupal\webauthn_framework\Exception;

/**
 * Invalid argument exception for the Webauthn Framework module.
 */
class WebauthnFrameworkInvalidArgumentException extends WebauthnFrameworkException {
}
