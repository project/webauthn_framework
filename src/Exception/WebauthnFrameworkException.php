<?php

declare(strict_types=1);

namespace Drupal\webauthn_framework\Exception;

/**
 * Exceptions for the Webauthn Framework module.
 */
class WebauthnFrameworkException extends \Exception {
}
