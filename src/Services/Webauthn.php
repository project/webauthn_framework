<?php

declare(strict_types=1);

namespace Drupal\webauthn_framework\Services;

use Cose\Algorithm\Manager;
use Cose\Algorithm\Signature\ECDSA\ES256;
use Cose\Algorithm\Signature\RSA\RS256;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\user\UserInterface;
use Drupal\webauthn_framework\Exception\WebauthnFrameworkException;
use Drupal\webauthn_framework\Exception\WebauthnFrameworkInvalidArgumentException;
use Drupal\webauthn_framework\WebauthnInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Webauthn\AttestationStatement\AttestationStatementSupportManager;
use Webauthn\AttestationStatement\NoneAttestationStatementSupport;
use Webauthn\AuthenticatorAssertionResponseValidator;
use Webauthn\AuthenticatorAttestationResponseValidator;
use Webauthn\CeremonyStep\CeremonyStepManagerFactory;
use Webauthn\Denormalizer\WebauthnSerializerFactory;
use Webauthn\PublicKeyCredential as PKCredential;
use Webauthn\PublicKeyCredentialOptions;
use Webauthn\PublicKeyCredentialRpEntity;
use Webauthn\PublicKeyCredentialSource;
use Webauthn\PublicKeyCredentialSource as PKCredentialSource;
use Webauthn\PublicKeyCredentialUserEntity;
use Webmozart\Assert\Assert;

/**
 * Helper class to use webauthn-lib with webauthn_framework module.
 */
final class Webauthn implements ContainerInjectionInterface, WebauthnInterface {
  use LoggerChannelTrait;

  /**
   * The serializer for converting Authenticator Responses to objects.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  private SerializerInterface $serializer;

  /**
   * The attestation response validator.
   *
   * @var \Webauthn\AuthenticatorAttestationResponseValidator
   */
  private AuthenticatorAttestationResponseValidator $authenticatorAttestationResponseValidator;

  /**
   * The assertion response validator.
   *
   * @var \Webauthn\AuthenticatorAssertionResponseValidator
   */
  private AuthenticatorAssertionResponseValidator $authenticatorAssertionResponseValidator;

  /**
   * The site configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Constructs a new Drupal\webauthn_framework\Services\Webauthn object.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected ModuleHandlerInterface $moduleHandler,
    protected RequestStack $requestStack,
  ) {
    $attestation_statement_support_manager = AttestationStatementSupportManager::create();
    $attestation_statement_support_manager->add(NoneAttestationStatementSupport::create());

    $serializer_factory = new WebauthnSerializerFactory($attestation_statement_support_manager);
    $this->serializer = $serializer_factory->create();

    $algorithm_manager = Manager::create()
      ->add(
        ES256::create(),
        RS256::create()
      );

    $csmFactory = new CeremonyStepManagerFactory();
    $csmFactory->setAlgorithmManager($algorithm_manager);

    $creationCSM = $csmFactory->creationCeremony();

    $this->authenticatorAttestationResponseValidator = AuthenticatorAttestationResponseValidator::create(
      $creationCSM
    );

    $requestCSM = $csmFactory->requestCeremony();
    $authenticatorAssertionResponseValidator = AuthenticatorAssertionResponseValidator::create(
      $requestCSM
    );
    $this->authenticatorAssertionResponseValidator = $authenticatorAssertionResponseValidator;

    $this->config = $this->configFactory->get('webauthn_framework.settings');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container): Webauthn {
    return new self(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('request_stack'),
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function authenticatorAssertionResponseValidator(): AuthenticatorAssertionResponseValidator {
    return $this->authenticatorAssertionResponseValidator;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function authenticatorAttestationResponseValidator(): AuthenticatorAttestationResponseValidator {
    return $this->authenticatorAttestationResponseValidator;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getRpId(): string {
    $relying_party_id_auto = $this->config->get('relying_party.id_auto');
    $relying_party_id = $this->config->get('relying_party.id');
    if ($relying_party_id_auto == TRUE) {
      $relying_party_id = $this->getDrupalDomain();
    }
    Assert::stringNotEmpty($relying_party_id, 'Failed to get relying_party.id!');
    return $relying_party_id;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function createRpEntity(): PublicKeyCredentialRpEntity {
    $relying_party_app_name = $this->config->get('relying_party.name');
    Assert::stringNotEmpty($relying_party_app_name, 'Failed to get relying_party.name!');
    $relying_party_id = $this->getRpId();
    $relying_party_logo = $this->config->get('relying_party.logo');
    if (!is_string($relying_party_logo)) {
      $relying_party_logo = NULL;
    }
    return PublicKeyCredentialRpEntity::create($relying_party_app_name, $relying_party_id, $relying_party_logo);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function createUserEntity(UserInterface $user): PublicKeyCredentialUserEntity {
    $user_uuid = $user->uuid();
    Assert::stringNotEmpty($user_uuid, 'Failed to get UUID for user!');

    $user_displayname = $user->getDisplayName();
    Assert::stringNotEmpty($user_displayname, 'Failed to get display name for user!');

    $webauthn_user_name = $user_displayname;

    if ($this->moduleHandler->moduleExists('email_registration')) {
      $user_email = $user->getEmail();
      Assert::stringNotEmpty($user_email, 'Failed to get user email!');
      $webauthn_user_name = $user_email;
    }

    // The displayName is set to the userName.
    // Different authenticators handle these two differently,
    // so we make them the same to minimize trouble.
    $webauthn_display_name = $webauthn_user_name;

    $webauthn_user_id = $user_uuid;
    $webauthn_icon = NULL;
    return new PublicKeyCredentialUserEntity("$webauthn_user_name", $webauthn_user_id, $webauthn_display_name, $webauthn_icon);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function createChallenge(): string {
    return random_bytes(16);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getPublicKeyCredentialFromResponse(string $response): PKCredential {
    $public_key_credential = $this->serializer->deserialize($response, PKCredential::class, 'json');
    if ($public_key_credential instanceof PKCredential) {
      return $public_key_credential;
    }
    throw new WebauthnFrameworkException('Failed to get public key credential from response!');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getPublicKeyCredentialSourceFromResponse(string $response): PKCredentialSource {
    $public_key_credential = $this->serializer->deserialize($response, PKCredentialSource::class, 'json');
    if ($public_key_credential instanceof PKCredentialSource) {
      return $public_key_credential;
    }
    throw new WebauthnFrameworkException('Failed to get public key credential source from response!');
  }

  /**
   * Gets the domain of the Drupal site request.
   *
   * @return string
   *   The domain of the Drupal site request.
   */
  private function getDrupalDomain(): string {
    $current_request = $this->requestStack->getCurrentRequest();
    if ($current_request instanceof Request) {
      return $current_request->getHost();
    }
    throw new WebauthnFrameworkInvalidArgumentException('Current request is not an HTTP request!');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function canDiscloseRegistration(): bool {
    $disclose = $this->config->get('disclose_registration');
    return $disclose === TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function serializePublicKeyCredential(PublicKeyCredentialOptions|PublicKeyCredentialSource $target): string {
    return $this->serializer->serialize(
      $target,
      'json',
      [
        AbstractObjectNormalizer::SKIP_NULL_VALUES => TRUE,
        JsonEncode::OPTIONS => JSON_THROW_ON_ERROR,
      ],
    );
  }

}
