<?php

declare(strict_types=1);

namespace Drupal\webauthn_framework\Exception;

/**
 * Unexpected value exception for the Webauthn Framework module.
 */
class WebauthnFrameworkUnexpectedValueException extends WebauthnFrameworkException {
}
