<?php

declare(strict_types=1);

namespace Drupal\webauthn_framework\Exception;

/**
 * Validation failed exception for the Webauthn Framework module.
 */
class WebauthnFrameworkValidationFailedException extends WebauthnFrameworkInvalidArgumentException {
}
