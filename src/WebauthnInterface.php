<?php

declare(strict_types=1);

namespace Drupal\webauthn_framework;

use Drupal\user\UserInterface;
use Webauthn\AuthenticatorAssertionResponseValidator;
use Webauthn\AuthenticatorAttestationResponseValidator;
use Webauthn\PublicKeyCredential as PKCredential;
use Webauthn\PublicKeyCredentialOptions;
use Webauthn\PublicKeyCredentialRpEntity;
use Webauthn\PublicKeyCredentialSource;
use Webauthn\PublicKeyCredentialSource as PKCredentialSource;
use Webauthn\PublicKeyCredentialUserEntity;

/**
 * Provides an interface for interacting with the Webauthn Framework.
 *
 * Based on the docs:
 * https://webauthn-doc.spomky-labs.com/pure-php/the-hard-way.
 */
interface WebauthnInterface {

  /**
   * Gets the authenticator ASSERTION response validator.
   *
   * @return \Webauthn\AuthenticatorAssertionResponseValidator
   *   The authenticator ASSERTION response validator.
   */
  public function authenticatorAssertionResponseValidator(): AuthenticatorAssertionResponseValidator;

  /**
   * Gets the authenticator ATTESTATION response validator.
   *
   * @return \Webauthn\AuthenticatorAttestationResponseValidator
   *   The authenticator ATTESTATION response validator.
   */
  public function authenticatorAttestationResponseValidator(): AuthenticatorAttestationResponseValidator;

  /**
   * Use the serializer to obtain a public key credential.
   *
   * @param string $response
   *   The response to convert to a public key credential.
   *
   * @return \Webauthn\PublicKeyCredential
   *   The public key credential.
   */
  public function getPublicKeyCredentialFromResponse(string $response): PKCredential;

  /**
   * Use the serializer to obtain a public key credential source.
   *
   * @param string $response
   *   The response to convert to a public key credential source.
   *
   * @return \Webauthn\PublicKeyCredentialSource
   *   The public key credential source.
   */
  public function getPublicKeyCredentialSourceFromResponse(string $response): PKCredentialSource;

  /**
   * Gets the relying party ID based on the site settings.
   *
   * Users can specify a domain or have the module automatically use the
   * site's current domain.
   *
   * @return string
   *   The relying party ID.
   */
  public function getRpId(): string;

  /**
   * Generates a challenge to prevent man-in-the-middle attacks.
   *
   * @return string
   *   The challenge for public key credential confirmation.
   */
  public function createChallenge(): string;

  /**
   * Generates an RP entity for the application (the Drupal site).
   *
   * @returns PublicKeyCredentialRpEntity
   *   The RP entity.
   */
  public function createRpEntity(): PublicKeyCredentialRpEntity;

  /**
   * Generates a webauthn user entity for the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The Drupal user account.
   *
   * @return \Webauthn\PublicKeyCredentialUserEntity
   *   The public key credential entity for webauthn.
   */
  public function createUserEntity(UserInterface $user): PublicKeyCredentialUserEntity;

  /**
   * Only show if an account has been registered if the site owner opts-in.
   *
   * This is a security/usability tradeoff.
   *
   * @return bool
   *   TRUE if the site owner allows email addresses to be disclosed.
   */
  public function canDiscloseRegistration(): bool;

  /**
   * Serializes the PublicKeyCredentialRequestOptions.
   *
   * Since webauthn-lib 5, we have to use Symfony serializer and cannot
   * use json_encode() or ->jsonSerialize().
   *
   * @param \Webauthn\PublicKeyCredentialOptions|\Webauthn\PublicKeyCredentialSource $target
   *   The options to serialize.
   *
   * @return string
   *   The options serialized as a string.
   */
  public function serializePublicKeyCredential(PublicKeyCredentialOptions|PublicKeyCredentialSource $target): string;

}
